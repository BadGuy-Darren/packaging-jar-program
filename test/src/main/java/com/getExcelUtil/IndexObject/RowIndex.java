package com.getExcelUtil.IndexObject;

import java.util.HashMap;
import java.util.Map;

public class RowIndex {

    Map<Integer, Integer[]> rowIndex = new HashMap<>();

    public void setRowIndex (Integer row, Integer[] columns) {
        rowIndex.put(row, columns);
    }

    public Map<Integer, Integer[]> getRowIndex() {
        return rowIndex;
    }

}
