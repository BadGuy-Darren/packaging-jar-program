package com.getExcelUtil.exceptions;

import java.util.List;

public class ExcelDataException extends RuntimeException {

    private final List<String> messages;

    public ExcelDataException(List<String> messages) {
        super(messages.get(0));
        this.messages = messages;
    }

    public List<String> getMessages() {
        return messages;
    }
}
