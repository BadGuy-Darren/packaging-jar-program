package com.getExcelUtil.exceptions;

public class GetExcelException extends RuntimeException {
    public GetExcelException(String msg) {
        super(msg);
    }
}
