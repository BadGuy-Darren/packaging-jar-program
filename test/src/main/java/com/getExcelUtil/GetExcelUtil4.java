package com.getExcelUtil;

import com.getExcelUtil.IndexObject.RowIndex;
import com.getExcelUtil.annotation.*;
import com.getExcelUtil.exceptions.ExcelDataException;
import com.getExcelUtil.exceptions.GetExcelException;
import org.apache.commons.beanutils.ConvertUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.hssf.usermodel.HSSFDateUtil;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.openxml4j.util.ZipSecureFile;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.lang.reflect.*;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @code 大迪
 * date 2021/3/5
 * @version 3.0
 * Description - GetExcelUtil 4.0
 *      1.适用于xlsx、xls
 *      2.(4.0)適用於橫向、縱向、<b>支持实体类接收数据</b>
 *      3.可配置起始行、列
 *      new: 4.实例化一次，多次取值；5.对象取值、list取值，可交叉取值
 * 泛型T：
 *      1. T 使用实体类时，实体类参数顺序与类型必须与模板栏位顺序、类型一致
 */
public class GetExcelUtil4 implements Serializable {
    private static final long serialVersionUID = 1L;

    private static final Logger log = LoggerFactory.getLogger(GetExcelUtil4.class);

    /**
     * Multipart
     */
    public static final int SAME_ROW = -1;
    public static final int SAME_COLUMN = -1;

    public final static int HORIZONTAL = 1;
    public final static int VERTICAL = 2;

    /**
     * 參數
     */
    private String datePattern = "yyyy/MM/dd";
    private String numberFormat = "#.##";

    /**
     * 返回值
     */
    private final List<String> messages = new ArrayList<>();

    private final Sheet sheet;


    public GetExcelUtil4(MultipartFile file) throws IOException {
        if (null == file) {
            throw new GetExcelException("请选择文件！");
        }
        String fileName = file.getOriginalFilename();
        assert fileName != null;
        ZipSecureFile.setMinInflateRatio(-1.0d);
        if (!fileName.matches("^.+\\.(?i)(xls)$") && !fileName.matches("^.+\\.(?i)(xlsx)$")) {
            log.warn("文件格式不正确!");
            throw new GetExcelException("文件格式不正确!");
        }

        // Mark try-with-resource机制，资源关闭交由java管理 （资源类需实现Closeable或AutoCloseable接口）
        try (InputStream inputStream = file.getInputStream();
             Workbook workbook = fileName.endsWith("xlsx") ?
                     new XSSFWorkbook(inputStream) : new HSSFWorkbook(inputStream);) {
            sheet = workbook.getSheetAt(0);
        }
    }


    /**
     * 设置日期格式
     */
    public GetExcelUtil4 setDatePattern(String datePattern) {
        this.datePattern = datePattern;
        return this;
    }


    /**
     * 设置数字格式
     */
    public GetExcelUtil4 setNumberFormat(String numberFormat) {
        this.numberFormat = numberFormat;
        return this;
    }


    public String getSheetName() {
        return sheet.getSheetName();
    }


    public List<String> getMessages() {
        return messages;
    }


    // list
    public List<List<Object>> getHorizontalData(int startRow, int startColumn, int cellNum) {
        List<List<Object>> list = new ArrayList<>();

        for (int i = startRow - 1; i <= sheet.getLastRowNum(); i++) {
            Row row = sheet.getRow(i);

            List<Object> li = new ArrayList<>();
            for (int m = startColumn - 1; m < startColumn - 1 + cellNum; m++) {
                Cell cell = row.getCell(m);
                Object rtCell = getCellVal(cell);

                li.add(rtCell);
            }
            list.add(li);
        }

        if (messages.size() > 0) {
            throw new ExcelDataException(messages);
        }

        if (list.size() == 0) {
            log.warn("选取的数据为空!");
            throw new GetExcelException("选取的数据为空!");
        }

        return list;
    }


    // 实体接收
    @SuppressWarnings("unchecked")
    public <T> List<T> getHorizontalData(int startRow, int startColumn, int cellNum, Class<T> clazz) {
        List<T> list = new ArrayList<>();
        if (isBaseType(clazz)) {
            for (int rowNum = startRow; rowNum <= sheet.getLastRowNum() + 1; rowNum++) {
                Row row = sheet.getRow(rowNum - 1);
                Cell cell = row.getCell(startColumn - 1);
                String cellVal = getCellVal(cell);

                list.add((T) ConvertUtils.convert(cellVal, clazz));
            }
        } else {
            for (int rowNum = startRow; rowNum <= sheet.getLastRowNum() + 1; rowNum++) {
                Row row = sheet.getRow(rowNum - 1);

                T t;
                try {
                    t = clazz.newInstance();
                } catch (InstantiationException | IllegalAccessException e) {
                    throw new RuntimeException(e);
                }
                Field[] fields = clazz.getDeclaredFields();

                int fieldsLen = fields.length;

                int fieldIndex = 0;

                int columnNum = startColumn;

                while (fieldIndex < fieldsLen) {

                    // 忽略接收实体类中的字段
                    if (fields[fieldIndex].isAnnotationPresent(ValueIngnore.class)) {
                        fieldIndex++;
                        continue;
                    }

                    // 附加取值区域
                    if (fields[fieldIndex].isAnnotationPresent(MultiArea.class)) {
                        setMultiArea(t, clazz, fields[fieldIndex], rowNum, columnNum);
                        fieldIndex++;
                        continue;
                    }

                    // 附加取值列
                    if (fields[fieldIndex].isAnnotationPresent(MultiColumns.class)) {
                        setMultiColumns(t, clazz, fields[fieldIndex], rowNum, columnNum);
                        fieldIndex++;
                        continue;
                    }

                    // 附加取值行
                    if (fields[fieldIndex].isAnnotationPresent(MultiRows.class)) {
                        setMultiRows(t, clazz, fields[fieldIndex], rowNum, columnNum);
                        fieldIndex++;
                        continue;
                    }

                    if (columnNum < startColumn + cellNum) {
                        if (!setField(t, clazz, fields[fieldIndex], row.getCell(columnNum - 1), rowNum, columnNum)) {
                            break;
                        }
                        columnNum++;
                        fieldIndex++;
                    }

                }

                list.add(t);
            }
        }
        if (messages.size() > 0) {
            throw new ExcelDataException(messages);
        }

        if (list.size() == 0) {
            log.warn("选取的数据为空!");
            throw new GetExcelException("选取的数据为空!");
        }

        return list;
    }


    // List<Object>
    public List<List<Object>> getVerticalData(int startRow, int startColumn, int cellNum) {
        List<List<Object>> list = new ArrayList<>();

        // 遍历sheet中的列
        for (int j = startColumn - 1; j <= sheet.getRow(startRow - 1).getLastCellNum(); j++) {

            List<Object> li = new ArrayList<>();

            for (int i = startRow - 1; i < startRow - 1 + cellNum; i++) {
                Row row = sheet.getRow(i);
                Cell cell = row.getCell(j);
                Object rtCell = getCellVal(cell);

                li.add(rtCell);
            }
            list.add(li);
        }

        if(messages.size()>0) {
            throw new ExcelDataException(messages);
        }

        if(list.size()==0) {
            log.warn("选取的数据为空!");
            throw new GetExcelException("选取的数据为空!");
        }

        return list;
    }


    @SuppressWarnings("unchecked")
    public <T> List<T> getVerticalData(int startRow, int startColumn, int cellNum, Class<T> clazz) {
        List<T> list = new ArrayList<>();

        if (isBaseType(clazz)) {
            for (int columnNum = startColumn; columnNum <= sheet.getRow(startRow - 1).getLastCellNum(); columnNum++) {
                Row row = sheet.getRow(startRow - 1);
                Cell cell = row.getCell(columnNum - 1);
                String cellVal = getCellVal(cell);

                list.add((T) ConvertUtils.convert(cellVal, clazz));
            }
        } else {
            // 遍历sheet中的列
            for (int columnNum = startColumn; columnNum <= sheet.getRow(startRow - 1).getLastCellNum(); columnNum++) {
                T t;
                try {
                    t = clazz.newInstance();
                } catch (InstantiationException | IllegalAccessException e) {
                    throw new RuntimeException(e);
                }
                Field[] fields = clazz.getDeclaredFields();

                int fieldsLen = fields.length;

                int fieldIndex = 0;

                int rowNum = startRow;

                while (fieldIndex < fieldsLen) {

                    // 忽略接收实体类中的字段
                    if (fields[fieldIndex].isAnnotationPresent(ValueIngnore.class)) {
                        fieldIndex++;
                        continue;
                    }

                    // MultiArea，附加区域
                    if (fields[fieldIndex].isAnnotationPresent(MultiArea.class)) {
                        setMultiArea(t, clazz, fields[fieldIndex], rowNum, columnNum);
                        fieldIndex++;
                        continue;
                    }

                    // MultiColumns，附加列
                    if (fields[fieldIndex].isAnnotationPresent(MultiColumns.class)) {
                        setMultiColumns(t, clazz, fields[fieldIndex], rowNum, columnNum);
                        fieldIndex++;
                        continue;
                    }

                    // MultiColumns，附加行
                    if (fields[fieldIndex].isAnnotationPresent(MultiRows.class)) {
                        setMultiRows(t, clazz, fields[fieldIndex], rowNum, columnNum);
                        fieldIndex++;
                        continue;
                    }

                    if (rowNum < startRow + cellNum) {
                        if (!setField(t, clazz, fields[fieldIndex], sheet.getRow(rowNum - 1).getCell(columnNum - 1), rowNum, columnNum)) {
                            break;
                        }
                        rowNum++;
                        fieldIndex++;
                    }

                }

                list.add(t);
            }
        }

        if (messages.size() > 0) {
            throw new ExcelDataException(messages);
        }

        if (list.size() == 0) {
            log.warn("选取的数据为空!");
            throw new GetExcelException("选取的数据为空!");
        }

        return list;
    }


    public List<Object> getDataByIndex(RowIndex rowIndex) {
        List<Object> li = new ArrayList<>();

        for (Integer rowNum : rowIndex.getRowIndex().keySet()) {
            Row row = sheet.getRow(rowNum - 1);
            for (Integer columnNum : rowIndex.getRowIndex().get(rowNum)) {
                Cell cell = row.getCell(columnNum - 1);
                Object rtCell = getCellVal(cell);

                li.add(rtCell);
            }
        }

        if (messages.size() > 0) {
            throw new ExcelDataException(messages);
        }

        // 数据内容判空
        if (li.size() == 0) {
            log.warn("选取的数据为空!");
            throw new GetExcelException("选取的数据为空!");
        }

        return li;
    }


    public <T> T getDataByIndex(RowIndex rowIndex, Class<T> clazz) {
        T t;
        try {
            t = clazz.newInstance();
        } catch (InstantiationException | IllegalAccessException e) {
            throw new RuntimeException(e);
        }
        Field[] fields = clazz.getDeclaredFields();

        int fieldsLen = fields.length;
        int fieldIndex = 0;

        for (Integer rowNum : rowIndex.getRowIndex().keySet()) {
            Row row = sheet.getRow(rowNum - 1);

            for (Integer columnNum : rowIndex.getRowIndex().get(rowNum)) {

                // 忽略接收实体类中的字段
                while (fieldIndex < fieldsLen && fields[fieldIndex].isAnnotationPresent(ValueIngnore.class)) {
                    fieldIndex ++ ;
                }

                while (fieldIndex < fieldsLen && fields[fieldIndex].isAnnotationPresent(MultiArea.class)) {
                    setMultiArea(t, clazz, fields[fieldIndex], rowNum, columnNum);
                    fieldIndex++;
                }

                while (fieldIndex < fieldsLen && fields[fieldIndex].isAnnotationPresent(MultiColumns.class)) {
                    setMultiColumns(t, clazz, fields[fieldIndex], rowNum, columnNum);
                    fieldIndex++;
                }

                while (fieldIndex < fieldsLen && fields[fieldIndex].isAnnotationPresent(MultiRows.class)) {
                    setMultiRows(t, clazz, fields[fieldIndex], rowNum, columnNum);
                    fieldIndex++;
                }

                // 确保忽略字段后能正常退出
                if (fieldIndex >= fieldsLen) {
                    break;
                }

                if (!setField(t, clazz, fields[fieldIndex], row.getCell(columnNum - 1), rowNum, columnNum)){
                    break;
                }

                fieldIndex ++ ;
            }

            if (fieldIndex >= fieldsLen) {
                break;
            }

        }

        while (fieldIndex < fieldsLen) {
            if (fields[fieldIndex].isAnnotationPresent(MultiArea.class)) {
                setMultiArea(t, clazz, fields[fieldIndex], -1, -1);
            } else if (fields[fieldIndex].isAnnotationPresent(MultiColumns.class)) {
                setMultiColumns(t, clazz, fields[fieldIndex], -1, -1);
            } else if (fields[fieldIndex].isAnnotationPresent(MultiRows.class)) {
                setMultiRows(t, clazz, fields[fieldIndex], -1, -1);
            }
            fieldIndex++;
        }

        if (messages.size() > 0) {
            throw new ExcelDataException(messages);
        }

        // 数据内容判空
        if (!ObjectUtils.allNotNull(t)) {
            log.warn("选取的数据为空!");
            throw new GetExcelException("选取的数据为空!");
        }

        return t;
    }

    // list
    public List<List<Object>> getColumns(int startRow, int[] columns) {

        // distinct去重
        List<Integer> columnList = Arrays.stream(columns).distinct().boxed().collect(Collectors.toList());

        List<List<Object>> list = new ArrayList<>();

        for (int i = startRow - 1; i <= sheet.getLastRowNum() + 1; i++) {
            Row row = sheet.getRow(i);

            List<Object> li = new ArrayList<>();
            for (Integer column : columnList) {
                Cell cell = row.getCell(column - 1);
                Object rtCell = getCellVal(cell);

                li.add(rtCell);
            }
            list.add(li);
        }

        if (messages.size() > 0) {
            throw new ExcelDataException(messages);
        }

        if (list.size() == 0) {
            log.warn("选取的数据为空!");
            throw new GetExcelException("选取的数据为空!");
        }

        return list;
    }

    // 实体接收
    public <T> List<T> getColumns(int startRow, int[] columns, Class<T> clazz) {

        // distinct去重
        List<Integer> columnList = Arrays.stream(columns).distinct().boxed().collect(Collectors.toList());

        List<T> list = new ArrayList<>();
        if (isBaseType(clazz)) {
            throw new RuntimeException("不支持的Class类型：" + clazz);
        } else {
            for (int rowNum = startRow; rowNum <= sheet.getLastRowNum() + 1; rowNum++) {
                Row row = sheet.getRow(rowNum - 1);

                T t;
                try {
                    t = clazz.newInstance();
                } catch (InstantiationException | IllegalAccessException e) {
                    throw new RuntimeException(e);
                }
                Field[] fields = clazz.getDeclaredFields();

                int fieldsLen = fields.length;

                int fieldIndex = 0;

                int columnIndex = 0;
                int cellNum = columnList.size();

                while (fieldIndex < fieldsLen) {

                    // 忽略接收实体类中的字段
                    if (fields[fieldIndex].isAnnotationPresent(ValueIngnore.class)) {
                        fieldIndex++;
                        continue;
                    }

                    // 附加取值区域
                    if (fields[fieldIndex].isAnnotationPresent(MultiArea.class)) {
                        setMultiArea(t, clazz, fields[fieldIndex], rowNum, columnList.get(columnIndex));
                        fieldIndex++;
                        continue;
                    }

                    // 附加列
                    if (fields[fieldIndex].isAnnotationPresent(MultiColumns.class)) {
                        setMultiColumns(t, clazz, fields[fieldIndex], rowNum, columnList.get(columnIndex));
                        fieldIndex++;
                        continue;
                    }

                    // 附加行
                    if (fields[fieldIndex].isAnnotationPresent(MultiRows.class)) {
                        setMultiRows(t, clazz, fields[fieldIndex], rowNum, columnList.get(columnIndex));
                        fieldIndex++;
                        continue;
                    }

                    if (columnIndex < cellNum) {
                        if (!setField(t, clazz, fields[fieldIndex], row.getCell(columnList.get(columnIndex) - 1), rowNum, columnList.get(columnIndex))) {
                            break;
                        }
                        columnIndex++;
                        fieldIndex++;
                    }

                }

                list.add(t);
            }
        }
        if (messages.size() > 0) {
            throw new ExcelDataException(messages);
        }

        if (list.size() == 0) {
            log.warn("选取的数据为空!");
            throw new GetExcelException("选取的数据为空!");
        }

        return list;
    }


    // List<Object>
    public List<List<Object>> getRows(int[] rows, int startColumn) {

        // distinct去重
        List<Integer> rowList = Arrays.stream(rows).distinct().boxed().collect(Collectors.toList());

        List<List<Object>> list = new ArrayList<>();

        // 遍历sheet中的列
        for (int j = startColumn - 1; j <= sheet.getRow(rowList.get(0) - 1).getLastCellNum(); j++) {

            List<Object> li = new ArrayList<>();

            for (Integer i : rowList) {
                Row row = sheet.getRow(i - 1);
                Cell cell = row.getCell(j);
                Object rtCell = getCellVal(cell);

                li.add(rtCell);
            }
            list.add(li);
        }

        if(messages.size()>0) {
            throw new ExcelDataException(messages);
        }

        if(list.size()==0) {
            log.warn("选取的数据为空!");
            throw new GetExcelException("选取的数据为空!");
        }

        return list;
    }


    public <T> List<T> getRows(int[] rows, int startColumn, Class<T> clazz) {

        // distinct去重
        List<Integer> rowList = Arrays.stream(rows).distinct().boxed().collect(Collectors.toList());

        List<T> list = new ArrayList<>();
        if (isBaseType(clazz)) {
            throw new RuntimeException("不支持的Class类型：" + clazz);
        } else {
            // 遍历sheet中的列
            for (int columnNum = startColumn; columnNum <= sheet.getRow(rowList.get(0) - 1).getLastCellNum(); columnNum++) {
                T t;
                try {
                    t = clazz.newInstance();
                } catch (InstantiationException | IllegalAccessException e) {
                    throw new RuntimeException(e);
                }
                Field[] fields = clazz.getDeclaredFields();

                int fieldsLen = fields.length;

                int fieldIndex = 0;

                int rowIndex = 0;
                int cellNum = rowList.size();

                while (fieldIndex < fieldsLen) {

                    // 忽略接收实体类中的字段
                    if (fields[fieldIndex].isAnnotationPresent(ValueIngnore.class)) {
                        fieldIndex++;
                        continue;
                    }

                    // MultiArea，附加区域
                    if (fields[fieldIndex].isAnnotationPresent(MultiArea.class)) {
                        setMultiArea(t, clazz, fields[fieldIndex], rowList.get(rowIndex), columnNum);
                        fieldIndex++;
                        continue;
                    }

                    // MultiColumns，附加列
                    if (fields[fieldIndex].isAnnotationPresent(MultiColumns.class)) {
                        setMultiColumns(t, clazz, fields[fieldIndex], rowList.get(rowIndex), columnNum);
                        fieldIndex++;
                        continue;
                    }

                    // MultiRows，附加行
                    if (fields[fieldIndex].isAnnotationPresent(MultiRows.class)) {
                        setMultiRows(t, clazz, fields[fieldIndex], rowList.get(rowIndex), columnNum);
                        fieldIndex++;
                        continue;
                    }

                    if (rowIndex < cellNum) {
                        if (!setField(t, clazz, fields[fieldIndex], sheet.getRow(rowList.get(rowIndex) - 1).getCell(columnNum - 1), rowList.get(rowIndex), columnNum)) {
                            break;
                        }
                        rowIndex++;
                        fieldIndex++;
                    }

                }

                list.add(t);
            }
        }
        if (messages.size() > 0) {
            throw new ExcelDataException(messages);
        }

        if (list.size() == 0) {
            log.warn("选取的数据为空!");
            throw new GetExcelException("选取的数据为空!");
        }

        return list;
    }


    /**
     * 是否满足实体类规则并赋值成功
     * @param o - 进行变量赋值的实体类
     * @param classType - 实体类的Class
     * @param field - 赋值字段
     * @param cell - 对应的Excel单元
     * @param rowNum - 行序号
     * @param columnNum - 列序号
     * @return 规则通过与否
     */
    private boolean setField(Object o, Class<?> classType, Field field, Cell cell, int rowNum, int columnNum) {

        String cellVal = getCellVal(cell, field);

        if (field.isAnnotationPresent(NotNull.class)) {
            NotNull valuePattern = field.getAnnotation(NotNull.class);
            if (StringUtils.isEmpty(cellVal)) {
                messages.add("文件第" + rowNum + "行第" + columnNum + "列：" + valuePattern.message());
                return false;
            }
        }

        if (field.isAnnotationPresent(ExcelDateFormat.class)) {
            ExcelDateFormat edf = field.getAnnotation(ExcelDateFormat.class);
            SimpleDateFormat sdf = new SimpleDateFormat(edf.pattern());
            try {
                sdf.parse(cellVal);
            } catch (ParseException e) {
                messages.add("文件第" + rowNum + "行第" + columnNum + "列：" + edf.message());
                return false;
            }
        }

        if (field.isAnnotationPresent(ExcelNumberFormat.class)) {
            ExcelNumberFormat enf = field.getAnnotation(ExcelNumberFormat.class);
            DecimalFormat df = new DecimalFormat(enf.format());
            try {
                df.format(cellVal);
            } catch (Exception e) {
                messages.add("文件第" + rowNum + "行第" + columnNum + "列：" + enf.message());
                return false;
            }
        }

        if (field.isAnnotationPresent(ValuePattern.class)) {
            ValuePattern valuePattern = field.getAnnotation(ValuePattern.class);
            String regexp = valuePattern.regexp();
            if (!cellVal.matches(regexp)) {
                messages.add("文件第" + rowNum + "行第" + columnNum + "列：" + valuePattern.message());
                return false;
            }
        }

        // 赋值
        setFieldValue(o, classType, field, cellVal);

        return true;
    }


    private void setMultiArea(Object o, Class<?> classType, Field field, int rowNum, int columnNum) {
        MultiArea multipart = field.getAnnotation(MultiArea.class);
        int startRow = multipart.startRow() == SAME_ROW ? rowNum : multipart.startRow();
        int startColumn = multipart.startColumn() == SAME_COLUMN ? columnNum : multipart.startColumn();

        ParameterizedType types = (ParameterizedType) field.getGenericType();
        Type type = types.getActualTypeArguments()[0];
        Object multipartObject;

        if (multipart.direction() == HORIZONTAL) {
            if (String.valueOf(type).equals("java.util.List<java.lang.Object>")) {
                multipartObject = getHorizontalData(startRow, startColumn, multipart.cellNum());
            } else if (type instanceof Class) {
                Class<?> clazz = (Class<?>) type;
                if (clazz.equals(classType)) throw new RuntimeException("非法的实体类引用：" + classType + "，可能造成死循环");
                multipartObject = getHorizontalData(startRow, startColumn, multipart.cellNum(), (Class<?>) type);
            } else {
                multipartObject = null;
            }
        } else if (multipart.direction() == VERTICAL) {
            if (String.valueOf(type).equals("java.util.List<java.lang.Object>")) {
                multipartObject = getVerticalData(startRow, startColumn, multipart.cellNum());
            } else if (type instanceof Class) {
                multipartObject = getVerticalData(startRow, startColumn, multipart.cellNum(), (Class<?>) type);
            } else {
                multipartObject = null;
            }
        } else {
            throw new RuntimeException("com.foxconn.indint.utils.getExcelUtil.annotation.Multipart：错误的direction值" + multipart.direction());
        }

        // 赋值
        setFieldValue(o, classType, field, multipartObject);
    }


    private void setMultiColumns(Object o, Class<?> classType, Field field, int rowNum, int columnNum) {
        MultiColumns multipart = field.getAnnotation(MultiColumns.class);
        int startRow = multipart.startRow() == SAME_ROW ? rowNum : multipart.startRow();
        int[] columns = multipart.columns();

        for (int i = 0; i < columns.length; i++) {
            if (columns[i] == SAME_COLUMN) {
                columns[i] = columnNum;
            }
        }
        // List<Integer> columns = Arrays.stream(multipart.columns()).distinct().boxed().collect(Collectors.toList());
        // Collections.replaceAll(columns, -1, columnNum);

        ParameterizedType types = (ParameterizedType) field.getGenericType();
        Type type = types.getActualTypeArguments()[0];
        Object multipartObject;

        if (String.valueOf(type).equals("java.util.List<java.lang.Object>")) {
            multipartObject = getColumns(startRow, columns);
        } else if (type instanceof Class) {
            Class<?> clazz = (Class<?>) type;
            if (clazz.equals(classType)) throw new RuntimeException("非法的实体类引用：" + classType + "，可能造成死循环");
            multipartObject = getColumns(startRow, columns, (Class<?>) type);
        } else {
            multipartObject = null;
        }

        // 赋值
        setFieldValue(o, classType, field, multipartObject);
    }


    private void setMultiRows(Object o, Class<?> classType, Field field, int rowNum, int columnNum) {
        MultiRows multipart = field.getAnnotation(MultiRows.class);
        int startColumn = multipart.startColumn() == SAME_ROW ? rowNum : multipart.startColumn();
        int[] rows = multipart.rows();

        for (int i = 0; i < rows.length; i++) {
            if (rows[i] == SAME_ROW) {
                rows[i] = columnNum;
            }
        }
        // List<Integer> columns = Arrays.stream(multipart.columns()).distinct().boxed().collect(Collectors.toList());
        // Collections.replaceAll(columns, -1, columnNum);

        ParameterizedType types = (ParameterizedType) field.getGenericType();
        Type type = types.getActualTypeArguments()[0];
        Object multipartObject;

        if (String.valueOf(type).equals("java.util.List<java.lang.Object>")) {
            multipartObject = getRows(rows, startColumn);
        } else if (type instanceof Class) {
            Class<?> clazz = (Class<?>) type;
            if (clazz.equals(classType)) throw new RuntimeException("非法的实体类引用：" + classType + "，可能造成死循环");
            multipartObject = getRows(rows, startColumn, (Class<?>) type);
        } else {
            multipartObject = null;
        }

        // 赋值
        setFieldValue(o, classType, field, multipartObject);
    }


    private void setFieldValue(Object target, Class<?> classType, Field field, Object value) {
        // 获取参数类
        Class<?>[] paramTypes = new Class[1];
        paramTypes[0] = field.getType();

        // 转化值
        Object[] values = new Object[1];
        values[0] = ConvertUtils.convert(value, paramTypes[0]);

        // 获取setter
        String fieldName = field.getName();
        String setNameMethodName = "set" + fieldName.substring(0, 1).toUpperCase() + fieldName.substring(1);
        Method setMethod;
        try {
            setMethod = classType.getDeclaredMethod(setNameMethodName, paramTypes);
        } catch (NoSuchMethodException e) {
            throw new RuntimeException(e);
        }

        // 赋值
        try {
            setMethod.invoke(target, values);
        } catch (IllegalAccessException | InvocationTargetException e) {
            throw new RuntimeException(e);
        }
    }


    private String getCellVal(Cell cell) {
        return getCellVal(cell, null);
    }


    private String getCellVal(Cell cell, Field field) {
        if (cell == null) {
            return "";
        }

        String cellString;

        switch (cell.getCellType()) {
            case STRING: // 字符串
                cellString = cell.getStringCellValue();
                break;
            case NUMERIC: // 数字
                if (HSSFDateUtil.isCellDateFormatted(cell)) {
                    //用于转化为日期格式
                    String pattern = null != field && field.isAnnotationPresent(ExcelDateFormat.class) ?
                            field.getAnnotation(ExcelDateFormat.class).pattern() : datePattern;
                    Date d = cell.getDateCellValue();
                    DateFormat f = new SimpleDateFormat(pattern);
                    cellString = f.format(d);
                } else {
                    // 用于格式化数字，只保留两位小数
                    String format = null != field && field.isAnnotationPresent(ExcelDateFormat.class) ?
                            field.getAnnotation(ExcelNumberFormat.class).format() : numberFormat;
                    DecimalFormat df = new DecimalFormat(format);
                    cellString = df.format(cell.getNumericCellValue());
                }
                break;
            case BOOLEAN: // Boolean
                cellString = String.valueOf(cell.getBooleanCellValue());
                break;
            case FORMULA: // 公式
                cellString = String.valueOf(cell.getCellFormula());
                // cellString = cell.getStringCellValue();
                break;
            case BLANK: // 空值
            case ERROR: // 故障
                cellString = "";
                break;
            default:
                cellString = "ERROR";
                break;
        }
        return cellString.trim();
    }

    private boolean isBaseType (Class<?> clazz) {
        return String.class.equals(clazz)
                || Integer.class.equals(clazz)
                || Double.class.equals(clazz)
                || Object.class.equals(clazz)
                || Long.class.equals(clazz);
    }

}
