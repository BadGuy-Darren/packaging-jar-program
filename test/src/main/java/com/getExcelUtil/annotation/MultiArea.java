package com.getExcelUtil.annotation;

import java.lang.annotation.*;

/**
 * 附加一个连贯的取值区域
 */
@Target(value = ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Inherited
public @interface MultiArea {

    /**
     * 附加部分起始行
     * -1 表示与被附加部分同行
     * @return 起始行
     */
    int startRow();

    /**
     * 附加部分起始列
     * -1 表示与被附加部分同列
     * @return 起始列
     */
    int startColumn();

    /**
     * 表头的单元格数
     * @return 表头的单元格数
     */
    int cellNum();

    /**
     * 表头方向
     * @return 1 水平/2 垂直
     */
    int direction();

}
