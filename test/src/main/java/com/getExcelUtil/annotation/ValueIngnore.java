package com.getExcelUtil.annotation;

import java.lang.annotation.*;

// 指定适用类型，类、方法等
@Target(value = ElementType.FIELD)
// 生命周期
@Retention(RetentionPolicy.RUNTIME)
// 指定自定义注解是否能随着被定义的java文件生成到JavaDoc文档当中
@Documented
// 指定某个自定义注解如果写在了父类的声明部分，那么子类的声明部分也能自动拥有该注解。测试拿掉该注解后，子类不能包含父类的@DataSourceFilter
@Inherited
public @interface ValueIngnore {
}
