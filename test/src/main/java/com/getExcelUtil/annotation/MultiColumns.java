package com.getExcelUtil.annotation;

import java.lang.annotation.*;

/**
 * 附加几个连贯的列
 */
@Target(value = ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Inherited
public @interface MultiColumns {

    /**
     * 附加部分起始行
     * -1 表示与被附加部分同行
     * @return 起始行
     */
    int startRow();

    /**
     * 附加列
     * -1 表示与被附加部分同列
     * @return 附加列
     */
    int[] columns();

}
