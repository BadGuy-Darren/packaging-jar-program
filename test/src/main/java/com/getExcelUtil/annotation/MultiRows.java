package com.getExcelUtil.annotation;

import java.lang.annotation.*;

/**
 * 附加几个连贯的行
 */
@Target(value = ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Inherited
public @interface MultiRows {

    /**
     * 附加行
     * -1 表示与被附加部分同行
     * @return 附加行
     */
    int[] rows();

    /**
     * 附加部分起始列
     * -1 表示与被附加部分同列
     * @return 起始列
     */
    int startColumn();

}
